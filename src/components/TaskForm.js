import React, { Component } from 'react';
import {connect} from 'react-redux';
import * as actions from './../actions/index';
class TaskForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            id: '',
            name: '',
            status: false
        };
    };
    componentWillMount() {
        if(this.props.itemEditing && this.props.itemEditing.id !== null){
            this.setState({
                id : this.props.itemEditing.id,
                name : this.props.itemEditing.name,
                status : this.props.itemEditing.status
            });
        }
    };
    // componentWillReceiveProps(nextProps) {
    //     if(nextProps && nextProps.itemEditing){
    //         this.setState({
    //             id : nextProps.itemEditing.id,
    //             name : nextProps.itemEditing.name,
    //             status : nextProps.itemEditing.status
    //         });
    //     }
    // }
    onCloseForm = () => {
        this.props.onCloseForm();
    };
    onSaveTask = (event) => {
        event.preventDefault();
        this.props.onSaveTask(this.state);
    };
    render() {
        if(!this.props.isDisplayForm) return null;
        return (
            <div className="panel panel-warning">
                <div className="panel-heading">
                    <h3 className="panel-title">
                        Thêm Công Việc
                        <span
                            className="fa fa-times-circle text-right"
                            onClick = {this.props.onCloseForm}
                        ></span>
                    </h3>
                </div>
                <div className="panel-body">
                    <form onSubmit={this.onSave} >
                        <div className="form-group">
                            <label>Tên :</label>
                            <input
                                type="text"
                                className="form-control"
                                name="name"
                            />
                        </div>
                        <label>Trạng Thái :</label>
                        <select
                            className="form-control"
                            name="status"
                        >
                            <option >Kích Hoạt</option>
                            <option value={false}>Ẩn</option>
                        </select><br/>
                        <div className="text-center">
                            <button type="submit" className="btn btn-warning" onClick={this.onSaveTask}>
                                <span className="fa fa-plus mr-5"></span>Lưu Lại
                            </button>&nbsp;
                            <button type="button" className="btn btn-danger">
                                <span className="fa fa-close mr-5"></span>Hủy Bỏ
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}
var mapStateToProps = (state) => {
    return {
        isDisplayForm: state.isDisplayForm,
        itemEditing : state.itemEditing
    }
};
var mapDispatchToProps = (dispatch, props) => {
    return {
        onCloseForm: () => {
            dispatch(actions.closeForm())
        },
        onSaveTask: (task) => {
            dispatch(actions.saveTask(task))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TaskForm);