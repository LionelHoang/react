export const isDisplayForm = 'isDisplayForm';
export const TOGGLE_FORM = 'TOGGLE_FORM';
export const CLOSE_FORM = 'CLOSE_FORM';
export const SAVE_TASK = 'SAVE_TASK';
export const EDIT_TASK = 'EDIT_TASK';