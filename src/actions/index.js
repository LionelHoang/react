import * as types from './../constants/ActionTypes';
export const toggleForm = () => {
    return {
        type: types.TOGGLE_FORM
    }
}
export const closeForm = () => {
    return {
        type: types.CLOSE_FORM
    }
}
export const saveTask = (task) => {
    return {
        type: types.SAVE_TASK,
        task
    }
}
export const editTask = (task) => {
    return {
        type: types.EDIT_TASK,
        task
    }
}